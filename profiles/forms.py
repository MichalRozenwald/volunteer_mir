# coding=utf-8
from django.forms import ModelForm, CharField, PasswordInput, ValidationError, ChoiceField,MultipleChoiceField
from django.db import models
from .models import UserProfile
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm, ReadOnlyPasswordHashField
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from registration.forms import RegistrationFormUniqueEmail
from tasktracker.models import Category

class ProfileForm(ModelForm):
    photo = models.ImageField(upload_to = 'user_photo')
    class Meta:
        model = UserProfile
        # exclude = ('user','last_name', 'fist_name',)
        fields = ('photo', 'information', 'about', 'telephone', 'category')
        # ADD Fields to change the existing info!!!

# class UserChangeForm(ModelForm):
#     """A form for updating users. Includes all the fields on
#     the user, but replaces the password field with admin's
#     password hash display field.
#     """
#     password = ReadOnlyPasswordHashField()
#
#     class Meta:
#         model = User
#         fields = ('email', 'password', 'date_of_birth', 'is_active', 'is_admin')
#
#     def clean_password(self):
#         # Regardless of what the user provides, return the initial value.
#         # This is done here, rather than on the field, because the
#         # field does not have access to the initial value
#         return self.initial["password"]
#
#
#
# class EditProfileForm(ModelForm):
#     first_name = CharField(label='First Name')
#     last_name = CharField(label='Last Name')
#
#     class Meta:
#         model = User
#         fields = ['first_name', 'last_name']
#
#
#
# class UserForm(UserChangeForm):
#     #password = ReadOnlyPasswordHashField(label="Password")
#     class Meta:
#         model = User
#         fields = ('last_name', 'first_name')
#         exclude = ('username', 'password', )
#
#     def clean_password(self):
#         return self.initial["password"]


class MyRegistrationForm(ModelForm):
    email = CharField(max_length=30, label='Почта')
    first_name = CharField(max_length=30, label='Имя')
    last_name = CharField(max_length=30, label='Фамилия')
    # username = CharField(max_length=30, label='Логин')
     #username = CharField(max_length=30, label='Логин') #, default='', blank=True)
    # my_username = CharField(max_length=100, default='', blank=True)
    # my_username = models.CharField(max_length=100, default='', blank=True)
    #password1 = CharField(label="Пароль", widget=PasswordInput(attrs={'placeholder': 'Password'}))
    #password2 = CharField(label="Подтверждение пароля", widget=PasswordInput(attrs={'placeholder': 'Confirm Password'}),
    #                      help_text = "Для подтверждения введите одинаковые пароли.")

    error_messages = {
        'password_mismatch': "The two password fields didn't match.",
        'email_mismatch': "Email addresses is not unique.",
    }
    password1 = CharField(label="Password",
        widget=PasswordInput)
    password2 = CharField(label="Password confirmation",
        widget= PasswordInput,
        help_text="Enter the same password as above, for verification.")


    class Meta:
        model = User
        fields = ['first_name','last_name','password1','password2', 'email']

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    # def clean_username(self):
    #     email = self.cleaned_data.get('email')
    #     username = email.split('@')[0]
    #     if email and User.objects.filter(email=email).exists():
    #         raise ValidationError(
    #             self.error_messages['email_mismatch'],
    #             code='email_mismatch')
    #     return username

    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = email.split('@')[0]
        if email and User.objects.filter(email=email).exists():
            raise ValidationError(
                self.error_messages['email_mismatch'],
                code='email_mismatch')
        return email

    def save(self, commit=True):
        user = super(MyRegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data["email"]
        # user.username = self.cleaned_data["username"]
        if commit:
            user.save()
        return user

    # department = forms.CharField(label="Department", max_length=100)

# coding=utf-8
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    url(r'^profile/edit/$', views.profile_edit, name='profile_edit'),
    url(r'^profile/$', views.profile_detail, name='profile_detail'),

    url(r'^welcome/$', views.welcome, name='welcome'),
    url(r'^about/$', views.about_info, name='about_info'),

    url(r'^registration/$', views.registration, name='registration'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



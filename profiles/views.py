# coding=utf-8
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import UserProfile
from tasktracker.models import Task
from .forms import ProfileForm, MyRegistrationForm #, UserForm, EditProfileForm


def about_info(request):  # !!
    """
    information about the project
    :param request:
    :return:
    """
    return render(request, 'about_info.html', {})


def welcome(request):
    """

    :param request:
    :return:
    """
    return render(request, 'welcome_page.html', {})
    # return render(request, '/home/michal/volunteer_mir/vol_mir/profiles/templates/profiles/profile_detail.html', {})


@login_required()
def profile_detail(request):
    """

    :param request:
    :return:
    """
    user_tasks = Task.objects.filter(author=request.user).order_by('published_date')

    all_tasks = Task.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    member_tasks = []

    for task in all_tasks:
        task.is_member = False
        task_members = set()
        for d in task.member.values('id'):
            task_members.add(d.get('id'))
        try:
            if request.user.id in task_members:
                task.is_member = True
                member_tasks.append(task)
        except:
            pass

    created_tasks = []
    if request.user.is_authenticated():
        created_tasks = Task.objects.filter(author=request.user).order_by('published_date')

    context = {
        'username': request.user.get_username(),
        'full_name': request.user.get_full_name(),
        'user': request.user,
        'user_tasks': user_tasks,
        'all_tasks': all_tasks,
        'member_tasks': member_tasks,
        'created_tasks': created_tasks,
    }

    return render(request, 'profiles/profile_detail.html', context=context)



def registration(request):
    """
    :param request:
    :return:
    """
    if request.method == "POST":
        user_form = MyRegistrationForm(request.POST)  # , instance=task)
        if user_form.is_valid():

            # UserProfile.objects.create_user(username=form.cleaned_data.get('username'),
            # password=form.cleaned_data.get('password'))
            new_email = user_form.clean_email()
            user_object = User.objects.create_user(
                                     password = user_form.cleaned_data.get('password1'),
                                     first_name = user_form.cleaned_data.get('first_name'),
                                     last_name = user_form.cleaned_data.get('last_name'),
                                     email = new_email,
                                     username = new_email.split('@')[0]
                                     )

            profile = UserProfile.objects.create(user=user_object, photo='user_photo/Photo_TAMPLATE_vol.jpg')

            user = authenticate(username= user_form.cleaned_data.get('email').split('@')[0],
                                password=user_form.cleaned_data.get('password1')
                                )
            if user is not None:  # add check if exist
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                error_msg = "Try one more time, please"
                return render(request, 'registration/registration.html', {'form': user_form, 'error_msg': error_msg})
    else:
        user_form = MyRegistrationForm()
    return render(request, 'registration/registration.html', {'form': user_form})




@login_required()
def profile_edit(request):
    """
    Edit profile for user
    :param request:
    :return:
    """
    args = {}

    if request.user.is_authenticated():
        user_prof = request.user.userprofile
        if request.method == 'POST':

            form = ProfileForm(request.POST, request.FILES,  instance=user_prof)
            form.actual_user = request.user

            if form.is_valid():
                #user_prof.category = form.cleaned_data.get('category')
                user_prof.photo = request.FILES.get('photo', user_prof.photo)
                user_prof.save()
                form.save()
                return HttpResponseRedirect('/profile')

        else:
            form = ProfileForm(instance=user_prof)

        args['form'] = form
        return render(request, 'profiles/profile_edit.html', args)
#
# def update_profile(request):
#     args = {}
#
#     if request.method == 'POST':
#         form = UpdateProfile(request.POST)
#         form.actual_user = request.user
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse('update_profile_success'))
#     else:
#         form = UpdateProfile()
#
#     args['form'] = form
#     return render(request, 'registration/update_profile.html', args)
#



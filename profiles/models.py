# coding=utf-8
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from tasktracker.models import Category


class UserProfile(models.Model):
    user = models.OneToOneField(User)  # , related_name=userprof, related_query_name=userprof) # request.user.userprof.
    photo = models.ImageField(upload_to='user_photo', default=None, null=True, blank=True)
    #photo = models.ImageField(upload_to='user_photo',url='/media/user_photo/Photo_TAMPLATE_vol.jpg')

    information = models.CharField(max_length=200, null=True, blank=True)
    about = models.TextField(blank=True, null=True)

    telephone = models.CharField(max_length=200, null=True, blank=True)
    contact_info = models.TextField(blank=True, null=True)
    address = models.TextField(blank=True, null=True)

    category = models.ManyToManyField(Category)

    def __str__(self):
        return self.name

    def set_info(self, info):
        """

        :param info:
        :return:
        """
        self.information = info
        return self.information

class Rating(models.Model):
    UserProfile = models.ForeignKey(UserProfile, related_name="author")
    rating = models.IntegerField(default=1) # number field

    def publish(self):
        """
        Publish a task = save to all tasks
        """
        self.save()

    def __str__(self):
        return self.rating

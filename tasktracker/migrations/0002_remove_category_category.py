# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-06-18 11:54
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasktracker', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='category',
        ),
    ]

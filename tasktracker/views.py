# coding=utf-8
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import Task, Comment, Category, Membership, User
from django.utils import timezone
from .forms import TaskForm, CommentForm
from django.shortcuts import redirect
from django.http.response import JsonResponse, HttpResponse
from django.core.exceptions import ObjectDoesNotExist

def task_list(request):
    """
    Shows in three tabs:
     all tasks,
    if user is authenticated:
        taken_tasks (member of wich the user is)
        created_tasks
    :param request:
    :return: task_list.html
    """
    all_tasks = Task.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    member_tasks = []

    for task in all_tasks:
        task.is_member = False
        task_members = set()
        for d in task.member.values('id'):
            task_members.add(d.get('id'))
        try:
            if request.user.id in task_members:
                task.is_member = True
                member_tasks.append(task)
        except:
            pass

    created_tasks = []
    if request.user.is_authenticated():
        #member_tasks = Task.objects.filter(member=request.user).order_by('published_date')
        created_tasks = Task.objects.filter(author=request.user).order_by('published_date')

    return render(request, 'tasktracker/task_list.html', {'user':request.user, 'all_tasks': all_tasks, 'member_tasks': member_tasks, 'created_tasks': created_tasks})


def task_detail(request, pk):
    """
    Details of chosen task
    :param request:
    :param pk:
    :return:
    """
    # task = get_object_or_404(Task, pk=pk)
    task = Task.objects.get(pk=pk)
    task.is_member = False
    task_members = set()
    for member in task.member.values('id'):
        task_members.add(member.get('id'))
    try:
        if request.user.id in task_members:
            task.is_member = True
    except:
        pass

    categories = task.category.all()
    return render(request, 'tasktracker/task_detail.html', {'task': task, 'categories': categories})


@login_required
def task_new(request):
    """

    :param request:
    :return:
    """

    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = Task();
            task = form.save(commit=False)
            task.author = request.user
            task.save()
            task.category = form.cleaned_data.get('category')
            # task.published_date = timezone.now()
            task.save()
            return redirect('tasktracker.views.task_detail', pk=task.pk)
    else:
        form = TaskForm()
    return render(request, 'tasktracker/task_edit.html', {'form': form})


@login_required
def task_edit(request, pk):
    """

    :param request:
    :param pk:
    :return:
    """

    task = get_object_or_404(Task, pk=pk)
    #categories = Category.objects.filter(user=request.user)

    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            #category = form.cleaned_data['category']
            task = form.save(commit=False)
            task.author = request.user
            task.published_date = timezone.now()
            #task.category = Category.objects.get(id__in=[int(form.cleaned_data.get('category'))])
            task.category = form.cleaned_data.get('category')
            task.save()
            return redirect('tasktracker.views.task_detail', pk=task.pk)
    else:
        form = TaskForm(instance=task)
    return render(request, 'tasktracker/task_edit.html', {'form': form})


    # user = request.user
    # categories = Category.objects.filter(user=request.user)
    # if request.method == 'POST':
    #     form = UserFeedForm(request.POST)
    #     form.fields['category'].queryset = categories
    #     if form.is_valid():
    #         feed = form.cleaned_data['feed']
    #         category = form.cleaned_data['category']
    #         title = form.cleaned_data['title']
    #         feed_obj, created = Feed.objects.get_or_create(feed_url=feed)
    #         obj = UserFeed(feed=feed_obj, title=title, category=category, user=user)
    #         obj.save()
    #     return HttpResponseRedirect("/reader/manage")
    # else:
    #     form = UserFeedForm()
    #     form.fields['category'].queryset = categories
    # context = {'page_title': page_title,
    #            'form': form,
    #            }
    # return expand_context_and_render(request, context, 'reader/form.html')


@login_required
def task_draft_list(request):
    """

    :param request:
    :return:
    """
    tasks = Task.objects.filter(published_date__isnull=True).order_by('created_date')
    return render(request, 'tasktracker/task_draft_list.html', {'tasks': tasks})


@login_required
def task_publish(request, pk):
    """

    :param request:
    :param pk:
    :return:
    """
    task = get_object_or_404(Task, pk=pk)
    task.publish()
    return redirect('tasktracker.views.task_detail', pk=pk)


@login_required
def task_remove(request, pk):
    """

    :param request:
    :param pk:
    :return:
    """
    task = get_object_or_404(Task, pk=pk)
    task.delete()
    return redirect('tasktracker.views.task_list')


@login_required
def add_comment_to_task(request, pk):
    """

    :param request:
    :param pk:
    :return:
    """
    task = get_object_or_404(Task, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.task = task
            comment.author = request.user
            comment.save()
            return redirect('tasktracker.views.task_detail', pk=task.pk)
    else:
        form = CommentForm()
    return render(request, 'tasktracker/add_comment_to_task.html', {'form': form})


@login_required
def comment_approve(request, pk):
    """

    :param request:
    :param pk:
    :return:
    """
    comment = get_object_or_404(Comment, pk=pk)
    comment.approve()
    return redirect('tasktracker.views.task_detail', pk=comment.task.pk)


@login_required
def comment_remove(request, pk):
    """

    :param request:
    :param pk:
    :return:
    """
    comment = get_object_or_404(Comment, pk=pk)
    task_pk = comment.task.pk
    comment.delete()
    return redirect('tasktracker.views.task_detail', pk=task_pk)


@login_required
def task_take_old(request, pk):
    """

    :param request:
    :param pk:
    :return:
    """
    data = {}

    try:
        task = get_object_or_404(Task, pk=pk)
        task.members.add(request.user)
        data['status'] = 'success'
    except(ObjectDoesNotExist):
        data['status'] = 'failed'
    return JsonResponse(data=data)

@login_required
def task_take(request):
    print("TASK TAKE")
    if request.method == "GET" and request.is_ajax():
        #request.session['task_take'] = request.GET['take_task']
        print("GET - task take ")
        pk = request.GET['task_pk']

    data = {}

    try:
        task = get_object_or_404(Task, pk=pk)
        #task.member.add(request.user)

        m = Membership(user=request.user, task=task)
        m.save()

        data['status'] = 'success'
    except(ObjectDoesNotExist):
        data['status'] = 'failed'
    return HttpResponse("Задача взята Вами!")#(<button id="task {{ task.pk }}" pk ="{{ task.pk }}" class="task_drop"> Участвовать!! </button>)


@login_required
def task_drop(request):
    print("TASK DROP")
    if request.method == "GET" and request.is_ajax():
        #request.session['task_take'] = request.GET['take_task']
        print("GET - task drop ")
        pk = request.GET['task_pk']

    data = {}

    try:
        task = get_object_or_404(Task, pk=pk)
        #task.member.add(request.user)
        #task.member.remove(request.user)
        m = Membership.objects.filter(task_id=task.id, user_id=request.user.id)  # get return many objects, when incorrect add to data_base - when page didn't update
        m.delete()

        data['status'] = 'success'
    except(ObjectDoesNotExist):
        data['status'] = 'failed'
    return HttpResponse("Задача взята Вами!")#(<button id="task {{ task.pk }}" pk ="{{ task.pk }}" class="task_drop"> Участвовать!! </button>)



def task_members(request, pk):
    """
    Shows in three tabs:
     all tasks,
    if user is authenticated:
        taken_tasks (member of wich the user is)
        created_tasks
    :param request:
    :return: task_list.html
    """

    task = get_object_or_404(Task, pk=pk)

    all_members = []
    wait_members = []
    finish_members = []

    print(task.membership_set.values())

    for member in task.membership_set.values():
        print(member)
        print(member.get('user_id'))
        member_user =  User.objects.get(id=member.get('user_id'))
        print(member.get('status'))

        all_members.append(member_user)

        if member.get('status') == 'taken': # wait_ap':
            wait_members.append(member_user)
        elif member.get('status') == 'finish':
            finish_members.append(member_user)

        # task_status = 0;
        # task.is_member = False
        # task_members = set()
        # task_members.add(member.get('status'))
        # try:
        #     if request.user.id in task_members:
        #         task.is_member = True
        #         member_tasks.append(task)
        # except:
        #     pass

    content = {
        'task': task,
        'user':request.user,
        'all_members': all_members,
        'wait_members': wait_members,
        'finish_members': finish_members,
    }
    return render(request, 'tasktracker/task_members_list.html', content)



@login_required
def task_approve(request):
    print("TASK APPROVE")
    if request.method == "GET" and request.is_ajax():
        #request.session['task_take'] = request.GET['take_task']
        pk = request.GET['task_pk']
        member_user_id = request.GET['member_user_id']
        print("GET - task approve ")


    data = {}

    try:
        task = get_object_or_404(Task, pk=pk)
        member = get_object_or_404(Membership, task_id= task.id, user_id=member_user_id)
        member.set_status('finish')
        member.save()

        data['status'] = 'success'
    except(ObjectDoesNotExist):
        data['status'] = 'failed'
    return HttpResponse("Задача взята Вами!")#(<button id="task {{ task.pk }}" pk ="{{ task.pk }}" class="task_drop"> Участвовать!! </button>)


# coding=utf-8
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.task_list, name='task_list'),
    url(r'^task/(?P<pk>[0-9]+)/$', views.task_detail, name='task_detail'),

    url(r'^task/new/$', views.task_new, name='task_new'),
    url(r'^task/(?P<pk>[0-9]+)/edit/$', views.task_edit, name='task_edit'),
    url(r'^drafts/$', views.task_draft_list, name='task_draft_list'),
    url(r'^task/(?P<pk>[0-9]+)/publish/$', views.task_publish, name='task_publish'),
    url(r'^task/(?P<pk>[0-9]+)/remove/$', views.task_remove, name='task_remove'),

    url(r'^task/task_take/$', views.task_take, name='task_take'),
    url(r'^task/task_drop/$', views.task_drop, name='task_drop'),
    url(r'^task/task_approve/$', views.task_approve, name='task_approve'),

    url(r'^task/(?P<pk>[0-9]+)/members/$', views.task_members, name='task_members'),


    url(r'^task/(?P<pk>\d+)/comment/$', views.add_comment_to_task, name='add_comment_to_task'),
    url(r'^comment/(?P<pk>\d+)/approve/$', views.comment_approve, name='comment_approve'),
    url(r'^comment/(?P<pk>\d+)/remove/$', views.comment_remove, name='comment_remove'),



]


# coding=utf-8
from __future__ import unicode_literals

from django.db import models

# Create your models here.

from django.utils import timezone
from django.contrib.auth.models import User



class Category(models.Model):
    title = models.CharField(max_length=200, default='Not Chosen')

    def __str__(self):
        return self.title


class Task(models.Model):
    author = models.ForeignKey(User, related_name="author")
    title = models.CharField(max_length=200)
    text = models.TextField()
    category = models.ManyToManyField(Category, default='Не выбрано')
    member = models.ManyToManyField(User, related_name="member", through='Membership')

    created_date = models.DateTimeField(
        default=timezone.now)
    published_date = models.DateTimeField(
        blank=True, null=True)

    event_date = models.DateTimeField(
        default=timezone.now)

    def publish(self):
        """
        Publish a task = save to all tasks
        """
        self.published_date = timezone.now()
        self.save()

    def add_member(self, member):
        self.member = member
        self.save()

    def drop_member(self, member):
        self.member = {}
        self.save()



    def __str__(self):
        return self.title

    def is_member(self, user):

        return False


class Comment(models.Model):
    task = models.ForeignKey('tasktracker.Task', related_name='comments')
    author = models.ForeignKey(User)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        """
        Save and publish a comment
        """
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text

    def approved_comments(self):
        """
        Publish a comment
        :return:
        """
        return self.comments.filter(approved_comment=True)


class Membership(models.Model):
    task = models.ForeignKey(Task)
    user = models.ForeignKey(User)
    status = models.CharField(default='taken', max_length=100) # 'taken', 'wait_approve', 'finished'
    date_joined = models.DateField(default=timezone.now)
    #invite_reason = models.CharField(max_length=64)
    #class Meta:
     #    auto_created = True
    def set_status(self, status):
        self.status = status
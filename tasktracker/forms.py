# coding=utf-8
from django import forms
from .models import Task, Comment, Category


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ('title', 'text', 'event_date', 'category')


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text',)

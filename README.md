# README #

Проект: **Платформа для волонтёров**

Содержание документа:

- Описание проекта

- Как развернуть проект

- Отчёт по проекту


#Описание проекта:

Платформа, которая дает возможность организациям, нуждающимся в волонтёрах, и людям, которые хотят посвятить своё время добрым делам, скоординироваться, найти друг друга.

В сервисе есть возможность размещать задачи, для выполнения которых нужны волонтёры. На главной странице находится лента всех кратких описаний задач. Задачи можно комментировать, задавать к ним вопросы. Пользователь, волонтёр, может стать участником задачи, нажав на странице ее описания кнопку, и эта задача добавляется в список его текущих мероприятий. Когда организатор подтвердит, что пользователь задачу выполнил, она переходит в список совершенных добрых дел.Пользователи и посты имеют систему рейтингов: пользователь за реализованную волонтёрскую задачу получает половину “звёздочки”, а если пользователя подписался выполнять задачу, но не сделал этого, получает штраф.


http://michalrozenwald.pythonanywhere.com/

# Как развернуть проект

$ git clone https://MichalRozenwald@bitbucket.org/MichalRozenwald/volunteer_mir.git

$ cd vol_mir

$ virtualenv --python=python3.4 vol_virenv

$ source myvenv/bin/activate

$ pip install -r requirements.txt

$ python manage.py makemigrations

$ python manage.py migrate

$ python manage.py runserver



# pythonanywhere.com

$ git clone https://MichalRozenwald@bitbucket.org/MichalRozenwald/volunteer_mir.git

$ cd vol_mir

$ virtualenv --python=python3.4 vol_virenv

$ source myvenv/bin/activate

(vol_virenv) $  pip install django whitenoise

(vol_virenv) $ python manage.py collectstatic

$ python manage.py migrate

$ python create superuser

# Отчёт по проекту:
https://docs.google.com/document/d/1nOZOdrybnOC36UH64hb0TgpqAmmt1fd5q1EbJsX-xjc/edit?usp=sharing

Промежуточный отчёт:

https://docs.google.com/document/d/1A7gPXJbjg6cZTS6nzQ7LGgNF9KC2_PSjqwb-kvIaekc/edit?usp=sharing
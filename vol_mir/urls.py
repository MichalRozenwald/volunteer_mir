# coding=utf-8
"""vol_mir URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import include, url
from django.contrib.auth.views import logout
from django.contrib.auth.views import login

from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    # Examples:
    # url(r'^$', 'vol_mir.views.home', name='home'),
    # url(r'^tasktracker/', include('tasktracker.urls')),

    # url(r'^admin/', admin.site.urls),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', login),
    url(r'^accounts/logout/$', logout, {'next_page': '/'}),
    #url(r'^accounts/logout/$', logout, {'next_page': '/'}),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'', include('tasktracker.urls')),

    url(r'', include('profiles.urls')),

    # url(r'^about/$', tasktracker.views.task_new, name='task_new'),

]


#
# URL MAP:
# michalrozenwald.pythonanywhere.com
#     admin/', include(admin.site.urls)),
#     /                               --- views.task_list

#    'tasktracker.urls' :

#     ^task/(?P<pk>[0-9]+)/            --- views.task_detail
#     ^task/new/                       --- views.task_new
#     ^task/(?P<pk>[0-9]+)/edit/       --- views.task_edit
#     ^drafts/                         --- views.task_draft_list
#
#     ^task/(?P<pk>[0-9]+)/publish/    --- views.task_publish
#     ^task/(?P<pk>[0-9]+)/remove/$    --- views.task_remove
#     ^task/(?P<pk>\d+)/comment/$      --- views.add_comment_to_task
#
#     ^comment/(?P<pk>\d+)/approve/$   --- views.comment_approve
#     ^comment/(?P<pk>\d+)/remove/$    --- views.comment_remove

# + User
#     ^accounts/login/$               --- 'django.contrib.auth.views.login'),
#     ^accounts/logout/$              --- 'django.contrib.auth.views.logout', {'next_page': '/'}),
#
#     ^profiles/                      --- include('profiles.urls')),
#     ^profiles/edit

# From the navi-bar + footer
# + about & contacts
#     ^about/$              --- about_info
#     ^

# + Tasks == /
